module.exports = function(sequelize, DataTypes) {

  const Settings = sequelize.define('settings', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    code: {
      type: DataTypes.STRING
    },
    name: {
      type: DataTypes.STRING
    },
    value: {
      type: DataTypes.STRING
    }
  })

  return Settings;
};
