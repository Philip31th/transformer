module.exports = function(sequelize, DataTypes) {

  const Histories = sequelize.define('daily_logs', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    total_comment_count : {
      type: DataTypes.INTEGER
    },
    date_commented: {
      type: DataTypes.DATEONLY
    }
  })

  return Histories;
};
