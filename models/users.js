
module.exports = function(sequelize, DataTypes) {

  const Users = sequelize.define('users', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    userId: {
      type: DataTypes.STRING
    },
    fullname: {
      type: DataTypes.STRING
    },
    username: {
      type: DataTypes.STRING
    },
    password: {
      type: DataTypes.STRING
    },
    Block: {
      type: DataTypes.STRING
    },
    Posted: {
      type: DataTypes.INTEGER
    },
    remarks: {
      type: DataTypes.STRING
    }
  })

  return Users;
};
