module.exports = function(sequelize, DataTypes) {

  const PostContents = sequelize.define('postcontents', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true,
      autoIncrement: true
    },
    content: {
      type: DataTypes.TEXT
    },
    imagepath: {
      type: DataTypes.STRING
    }
  })

  return PostContents;
};
