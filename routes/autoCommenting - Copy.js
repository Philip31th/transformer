const router = require('express').Router()
const shuffle = require('shuffle-array')
const chromeLauncher = require('chrome-launcher')
const { Chromeless } = require('chromeless')
const publicIp = require('public-ip')
// const shuffle = require('shuffle-array')
const Excel = require('exceljs')
const Sequelize = require('sequelize')
const sequelize = new Sequelize('twitter_posting','root','', {
  host: 'localhost',
  port: 3306,
  dialect: 'mysql',
  dialectOptions: {
    multipleStatements: true
  },
  define: {
    timestamps: false
  },

  pool: {
    max: 5,
    min: 0,
    idle: 1000
  }
})
const path = require('path')
const mime = require('mime')
const fs = require('fs')
const os = require('os')
const usersModel = require('./../models/users.js')(sequelize, Sequelize)
const PostContents = require('./../models/postcontents.js')(sequelize, Sequelize)
const Settings = require('./../models/settings.js')(sequelize, Sequelize)
// const keywordsModel = require('./../models/keywords.js')(sequelize, Sequelize)
// const historiesModel = require('./../models/histories.js')(sequelize, Sequelize)
const socket = require('socket.io-client')('http://' + getLocalIP() + ':7050')
const AutoRegistration = require('./../lib/AutoRegistration')
const uuidv1 = require('uuid/v1')

var _autoRegistration
var _chrome
var _chromeless
var _username = 'chloebegum13@twitter10.com'
var _password = 'chloebegum13chloebegum13'
var _postMessage = `국내 최고 스포츠 배팅사이트 ▶ gta-ss.com ◀
오픈톡문의 : gta365
#카지노
#온라인카지노
#축구배팅
#카지노게임
#라이브카지노
#스포츠배팅사이트
#스포츠토토
#스포츠배팅
#스포츠오드스`

var _commentMessage = `클릭 ▶☃ gta-ss.com  ☃
✔코드:gta38
☀철통보안☀
Ⓜ안전공원인증Ⓜ
안전 놀이터
단폴O 크로스O
LTE급환전- 
✏가입첫충10% ✏
https://twitter.com/newskoreaupdate

#메이저사이트
#메이저놀이터
#안전한놀이터
#실시간게임
#해외스포츠
#먹튀없는곳
#비트코인
#챔피언스리스
#축구픽
#농구픽`
var _photo = ''
var _emoticon = ['☃', '✔', '☀', 'Ⓜ', '✏', '◀']
var _trendTopicId = ''
var _trendTopic = ''
var _noOfTopic = 10
var _retweetCount = 10
var _maxCommentCount = 20
var _commentCount = 0
var _successCommentCount = 0
var _unsuccessCommentCount = 0
var _users = ''
var _usersCount = 0
var _index = 0
var _totalCommentCount = 0
var _maxTotalCommentCount = 50
var __postMessage = _postMessage
var _firstPost = 0
var _fullname = ''
var _posts = ''
var _postsCount = 0
let _imageCounter = 0
let _postCounterLimit = 0
let _lastProfile = ''



router.post('/stop', async (req, res) => {
    await _chrome.kill()
    process.exit()
})

socket.on('request data', () => {

    Settings.findById(2).then(datas =>{
      _postCounterLimit = datas.value 
    })

    Settings.findById(1).then(datas =>{
      _lastProfile = datas._lastProfile 
    })

    usersModel.findAndCountAll().then(datas => {
      _users = datas.rows
      _usersCount = datas.count - 1

      usersModel.findAll(/* where: {title: 'aProject'} */).then(data => {
        socket.emit('fetch users', { users: _users, users: data })
      }).catch(err => {
        socket.emit('fetch users', { users: _users, users: [] })
      })

    }).catch(err => {
      socket.emit('fetch users', { users: [] })
    })
    // posting
    PostContents.findAndCountAll().then(datas => {
      _posts = datas.rows
      _postsCount = datas.count - 1

      PostContents.findAll(/* where: {title: 'aProject'} */).then(data => {
        socket.emit('fetch posts', { posts: _posts, posts: data })
      }).catch(err => {
        socket.emit('fetch posts', { posts: _posts, posts: [] })
      })
    }).catch(err => {
      socket.emit('fetch posts', { users: [] })
    })
})

router.get('/', (req, res) => {

    publicIp.v4().then((ip) => {
      console.log(ip)
    })

    res.render('main', {
      username: _username,
      password: _password,
      postMessage: _postMessage,
      commentMessage: _commentMessage,
      photo: _photo,
      noOfTopic: _noOfTopic,
      retweetCount: _retweetCount,
      maxCommentCount: _maxCommentCount,
      localIP: getLocalIP()
    })

})

router.post('/changePostContent',(req,res) => {
 let data = JSON.parse(req.body.data)
 PostContents.update(
    { content: data.content , imagepath : data.imagePath},
    { where: { id: data.id } }
  )
  .then(result => {
     if(result){
      if(req.files != null){
        req.files.attachment.mv('public' + '/images/' + data.imagePath, function(err) {
          if(err)
            return res.status(500).send(err);
          res.send('File uploaded!');
        });  
      }
     }

    })
  .catch(err =>
    console.log(err)
  )
  //console.log(req.files.attachment)
  
  console.log(data)
  /*req.files.attachment.mv('public' + '/images/' + 'hello' + '.jpg', function(err) {
    if (err)
      return res.status(500).send(err);
    res.send('File uploaded!');
  });*/

})

router.post('/keywords', (req, res) =>  {
    switch (req.body.action) {
      case 'Insert':

        sequelize.query(`
          INSERT INTO keywords(id, keyword)
          VALUES(:id, :keyword);
        `, {
          type: sequelize.QueryTypes.INSERT,
          replacements: {
            id: req.body.id,
            keyword: req.body.keyword
          }
        }).then(users => {

           res.json({ refresh: true })

           keywordsModel.findAndCountAll().then(datas => {
             _keywords = datas.rows
             _keywordsCount = datas.count - 1

             socket.emit('fetch keywords', { keywords: _keywords })

           }).catch(err => {
             socket.emit('fetch keywords', { keywords: [] })
           })

        }).catch(err => {
          console.log(err)
        })

        break

      case 'Update':

        sequelize.query(`
          UPDATE users SET username=:keyword WHERE id=:id;
        `, {
          type: sequelize.QueryTypes.UPDATE,
          replacements: {
            id: req.body.id,
            keyword: req.body.keyword
          }
        }).then(users => {

           res.json({ refresh: true })

           usersModel.findAndCountAll().then(datas => {
            _users = datas.rows
            _usersCount = datas.count - 1

            usersModel.findAll().then(data => {
              socket.emit('fetch users', { users: data })
            }).catch(err => {
              socket.emit('fetch users', { users: [] })
            })

          }).catch(err => {
            socket.emit('fetch users', { users: [] })
          })

        }).catch(err => {
          console.log(err)
        })

        break

      case 'Delete':

        sequelize.query(`
          DELETE FROM users WHERE id=:id;
        `, {
          type: sequelize.QueryTypes.DELETE,
          replacements: {
            id: req.body.id
          }
        }).then(users => {

          res.json({ refresh: true })

          usersModel.findAndCountAll().then(datas => {
            _users = datas.rows
            _usersCount = datas.count - 1

            usersModel.findAll().then(data => {
              socket.emit('fetch users', { users: data })
            }).catch(err => {
              socket.emit('fetch users', { users: [] })
            })

          }).catch(err => {
            socket.emit('fetch users', { users: [] })
          })

        }).catch(err => {
          console.log(err)
        })

        break

      case 'DeleteAll':

        sequelize.query(`
          DELETE FROM keywords;
        `, {
          type: sequelize.QueryTypes.DELETE,
          replacements: {
            id: req.body.id
          }
        }).then(users => {

          res.json({ refresh: true })

          keywordsModel.findAndCountAll().then(datas => {
            _keywords = datas.rows
            _keywordsCount = datas.count - 1

            socket.emit('fetch keywords', { keywords: _keywords })

          }).catch(err => {
            socket.emit('fetch keywords', { keywords: [] })
          })

        }).catch(err => {
          console.log(err)
        })

        break
    }
})

function getLocalIP() {

  var interfaces = os.networkInterfaces();
  var addresses = [];

  for (var k in interfaces) {
      for (var k2 in interfaces[k]) {
          var address = interfaces[k][k2];
          if (address.family === 'IPv4' && !address.internal) {
              addresses.push(address.address);
          }
      }
  }

  return addresses[0]

}

function generatePost() {
  


}

function addUser(fullname, username, password) {
  sequelize.query(`
    INSERT INTO users(id, fullname, username, password)
    VALUES(UUID(), :fullname, :username, :password);
  `, {
    type: sequelize.QueryTypes.INSERT,
    replacements: {
      fullname: fullname,
      username: username,
      password: password
    }
  }).then(users => {
    return true
  }).catch(err => {
    return true
    console.log(err)
  })
}

function updatePostStatus(id, Posted) {
  sequelize.query(`
    UPDATE users SET Posted=:Posted WHERE id=:id;
  `, {
    type: sequelize.QueryTypes.UPDATE,
    replacements: {
      id: id,
      Posted: Posted
    }
  }).then(users => {

    console.log('User Updated.')

    usersModel.findAndCountAll().then(datas => {
      _users = datas.rows
      _usersCount = datas.count - 1

      usersModel.findAll().then(data => {
        socket.emit('fetch users', { users: data })
      }).catch(err => {
        socket.emit('fetch users', { users: [] })
      })

    }).catch(err => {
      socket.emit('fetch users', { users: [] })
    })

  }).catch(err => {
    console.log(err)
  })
}

router.post('/uploadPromos', function(req, res) {

  var status

  if (req.files.excelFile != null) {
      let excelFile = req.files.excelFile

      excelFile.mv('uploaded_excelfile/UserAccounts.xlsx', (err) => {

        if (err) {
          console.log(err)
          res.status(404)
          res.send(err)
        } else {
          console.log('File uploaded.')

          var excelImportation = new Promise(function (resolve, reject) {

            var workbook = new Excel.Workbook()
            workbook.xlsx.readFile('uploaded_excelfile/UserAccounts.xlsx')
                .then(() => {
                    var worksheet = workbook.getWorksheet(1)
                    var headerRow = worksheet.getRow(1)

                    // if (headerRow.getCell('A').value == 'Username' || headerRow.getCell('B').value == 'Title' || headerRow.getCell('C').value == 'Body') {

                        worksheet.eachRow({ includeEmpty: false }, function(row, rowNumber) {
                          if (rowNumber > 1) {
                            addUser(row.values[1], row.values[2], row.values[3])
                          }
                        })

                        resolve(200)

                    // } else {
                    //     reject(422)
                    // }
                })

          })

          excelImportation.then(function () {
              res.status(200)
              res.send('File uploaded.')
          }).catch(function () {
              res.status(422)
              res.send('File is invalid.')
          })

        }

      })
  } else {
    res.status(404)
    res.send('No file uploaded.')
  }

})

router.post('/export', async (req, res) => {

    var excelPath = 'export_excelfile/UserAccounts.xlsx'
    var workbook = new Excel.Workbook()
    console.log(excelPath)
    workbook.xlsx.writeFile(excelPath)
    .then(function() {
      workbook.views = [
        {
          x: 0, y: 0, width: 10000, height: 20000,
          firstSheet: 0, activeTab: 1, visibility: 'visible'
        }
      ]

      var worksheet = workbook.addWorksheet('UserAccounts')

      worksheet.columns = [
          { header: 'User ID', key: 'userId', width: 15 },
          { header: 'Fullname', key: 'fullname', width: 20 },
          { header: 'Username', key: 'username', width: 40 },
          { header: 'Password', key: 'password', width: 15},
          { header: 'Block', key: 'Block', width: 10 }
      ]

      usersModel.findAll().then(async (data) => {

        data.forEach(async (item, index) => {
          await worksheet.addRow({
            userId: item.userId, 
            fullname: item.fullname,
            username: item.username,
            password: item.password, 
            Block: item.Block
          })

        })

        workbook.xlsx.writeFile(excelPath)

        res.download(excelPath)

      }).catch((error) => {
        console.log(error)
        res.status(422)
        res.send(error)
      })
    })

})

router.post('/start', async (req, res) => {
    console.log(_users[0].Posted,_postCounterLimit )
    //console.log(req.body.externalIP)
    for(i = 0 ; i <= _posts.length ; i++){
      if(_users[i].Posted  == _postCounterLimit){
        _index++;
      }
    }
    console.log(_users[_index].username)
    _username = req.body.username
    _password = req.body.password
    _postMessage = req.body.postMessage
    _commentMessage = req.body.commentMessage
    _photo = req.body.photo
    _noOfTopic = req.body.noOfTopic
    _retweetCount = req.body.retweetCount
    _maxCommentCount = req.body.maxCommentCount

    res.json({
      username: _username,
      password: _password,
      postMessage: _postMessage,
      commentMessage: _commentMessage,
      photo: _photo,
      noOfTopic: _noOfTopic,
      retweetCount: _retweetCount,
      maxCommentCount: _maxCommentCount,
      localIP: getLocalIP()
    })
    _imageCounter = _imageCounter + _users[_index].Posted
    __postMessage = _posts[_index].content

    await startLogin()

})

function startLogin() {

    return new Promise(async (resolve, reject) => {

      _chrome = await chromeLauncher
      .launch({
        port: 9222,
        chromeFlags: ['--disable-gpu']
      })

      _chromeless = new Chromeless({ waitTimeout: 50 * 1000 })

      socket.emit('updateLoggedUserInfo', {
        username: _users[_index].username,
        password: _users[_index].password,
        totalPost: _users[_index].Posted,
      })
      console.log('User Information')
      console.log('Fullname: ' + _users[_index].fullname)
      console.log('Username: ' + _users[_index].username)
      console.log('Password: ' + _users[_index].password)
      console.log('Post Count: ' + _users[_index].Posted)
      console.log('Post Content: ' + _posts[_index].content)
      console.log('Post Photo: ' + _posts[_imageCounter].imagepath)

      // if(_users[_index].Posted >= 40) {

      //   _index += 1
        
      //   await _chrome.kill()

      //   return startLogin()

      // } else {

      let isLogin = await initializeBrowsing()

      //   if(isLogin) {

      //     isPosted = await processPosting()

      //   }

      // }
      if(_users[_index].Posted  == _postCounterLimit) {
        _imageCounter = 0
        _index++
        await _chrome.kill()
        Settings.update(
          { value: _users[_index].Posted},
          { where: { id: 3 } }
        ).then(result =>{
    
        })
        console.log('Logging Out')
        return startLogin()

      }

      resolve(true)

    })
    .then(async () => {

      // await _chromeless
      // .evaluate(() => {
      //   document.querySelector('#user-dropdown > div.DashUserDropdown.dropdown-menu.dropdown-menu--rightAlign.is-forceRight.is-autoCentered > ul > li#signout-button.js-signout-button > button.dropdown-link').click()
      // })
      // .then(async () => {

      //   console.log(_users[_index].username + ' Logging Out.')

      //   _index += 1
        
      //   await _chrome.kill()

      //   return startLogin()

      if(_users[_index].Posted  == _postCounterLimit) {
        _imageCounter = 0
        _index++
        await _chrome.kill()
        Settings.update(
          { value: _users[_index].Posted},
          { where: { id: 3 } }
        ).then(result =>{
          
        })
        console.log('Logging Out')
        return startLogin()
      }

      processPosting()

      // })

    })
    .catch(async (err) => {

      console.log(err)

      await _chrome.kill()

      return startLogin()

    })

}

function initializeBrowsing() {

    return new Promise(async (resolve, reject) => {

      console.log('Logging In')

      await _chromeless
      .goto('https://twitter.com/login?lang=ko')
      .wait(10000)
      .clearInput('#page-container > div > div.signin-wrapper > form > fieldset > div:nth-child(2) > input')
      .clearInput('#page-container > div > div.signin-wrapper > form > fieldset > div:nth-child(3) > input')
      .wait(5000)
      .type(_users[_index].username, '#page-container > div > div.signin-wrapper > form > fieldset > div:nth-child(2) > input')
      .wait(5000)
      .type(_users[_index].password, '#page-container > div > div.signin-wrapper > form > fieldset > div:nth-child(3) > input')
      .wait(5000)
      .click('#page-container > div > div.signin-wrapper > form > div.clearfix > button')
      .wait(10000)
      .exists('#search-query')
      .then(async (searchElement) => {
        console.log(searchElement)
        if(searchElement) {

          console.log('Search Element: ' + searchElement)

          console.log(_users[_index].username + ' Logged In.')
          
          resolve(true)

        } else {

          console.log('Search Elemente: ' + searchElement)

          await _chromeless
          .exists('#challenge_response')
          .then(async (challengeElement) => {

            if(challengeElement) {

              console.log('Challenge Element: ' + challengeElement)

              await _chromeless
              .wait(10000)
              .clearInput('#challenge_response')
              .type(_users[_index].userId, '#challenge_response')
              .wait(10000)
              .click('#email_challenge_submit')
              .wait(10000)
              .click('#promptbird-modal-prompt-dialog > div.modal-content > div > div > div > button')
              .wait(15000)
              .exists('#search-query')
              .then((res) => {

                resolve(res)

              })
              .catch((err) => {

                resolve(false)

              })

            } else {

              console.log('Challenge Element: ' + challengeElement)
              _imageCounter = 0
              _index++
              await _chrome.kill()

              return startLogin()
              // Block Element

              // console.log('Account Blocked')

              // sequelize.query(`
              //   UPDATE users SET Block = :Block WHERE id = :id;
              // `, {
              //   type: sequelize.QueryTypes.UPDATE,
              //   replacements: {
              //     id: _users[_index].id,
              //     Block: 'True'
              //   }
              // })
              // .then(users => {
              //   console.log('User Updated')
              //  })
                         
            }

          })
          .catch(() => {

            console.log('Test Error')

          })

        }

      })
      .catch((error) => {

        console.log(error)
        resolve(false)

      })
    })
    .then((result) => {

      console.log(result)
      return true

    })
    .catch((error) => {


      return initializeBrowsing()

    })

}
let likePost = () => {

  return new Promise(async (resolve, reject) => {
    //console('https://twitter.com/' + _lastProfile)
  await _chromeless
    .goto('https://twitter.com/joe')
    .wait(5000)
    .evaluate((counter)=>{
      for(i = 1 ; i < counter ; i++){
        console.log("test:"+i+"")
        document.querySelector(".js-stream-item.stream-item.stream-item:nth-child("+i+") .ProfileTweet-actionList.js-actions .ProfileTweet-actionButton.js-actionButton.js-actionFavorite").click()
      }  
    },_users[_index].Posted)
    .wait(5000)
    //.click(".js-stream-item.stream-item.stream-item:nth-child(1) .ProfileTweet-actionList.js-actions .ProfileTweet-actionButton.js-actionButton.js-actionFavorite")
    .then(() =>{

        /*resolve(likePost)
        await _chrome.kill()
        return startLogin()*/
       /* if(results){
          resolve(true)*/
          //console.log(results)
          /*await _chromeless
          .click(".js-stream-item.stream-item.stream-item:nth-child(1) .ProfileTweet-actionList.js-actions .ProfileTweet-actionButton.js-actionButton.js-actionFavorite")*/

          /*for(i = 1 ; i < 10 ; i++){*/
  /*          await _chromeless
            .wait(5000)
            .click(".js-stream-item.stream-item.stream-item:nth-child(1) .ProfileTweet-actionList.js-actions .ProfileTweet-actionButton.js-actionButton.js-actionFavorite")
            .wait(5000)
            .then( async(results) =>{*/
             // await _chromeless.scrollTo(0, 500)
      /*        .then((res)=>{
                resolve(true)
              })
              .catch(()=>{

              })*/
  /*          })
            .catch((err)=>{
               console.log('Error')
            })*/
          //}
       //}
  //      resolve(true)
    })
    .catch((err)=>{
      console.log(err)
    })

  })
  .then(async(results) =>{
    resolve(true)
    await _chrome.kill()

    return startLogin()
  })
}
var processPosting = () => {

  return new Promise(async (resolve, reject) => {
    socket.emit('updateLoggedUserInfo', {
      username: _users[_index].username,
      password: _users[_index].password,
      totalPost: _users[_index].Posted,
    })    
    console.log('Post Count: ' + _users[_index].Posted)

    console.log('Postings...')

    let photo = path.resolve('public','images',_posts[_imageCounter].imagepath) 

    console.log(photo)
    console.log(_index)

    //shuffle(photo)

    await _chromeless
    .wait(5000)
    .evaluate((postM) => {
      document.querySelector('#tweet-box-home-timeline > div').innerHTML = postM.replace(/\n{2,}/g, "</p><br/><p>").replace(/\n/g, "<br/>")
    }, __postMessage)
    .wait(5000)
    .setFileInput('#timeline > div.timeline-tweet-box > div > form > div.tweet-content > div.TweetBox-photoIntent > div > div > label > input', photo)
    .wait(5000)
    .evaluate(() => {
      document.querySelector('#timeline > div.timeline-tweet-box > div > form > div.TweetBoxToolbar > div.TweetBoxToolbar-tweetButton.tweet-button > button > span').click()
    })
    .wait(15000)
    .then(() => {

      setTimeout(function() {
        resolve(true)
      }, 10000)

    })
    .catch(() => {

      console.log('Posting Failed!')
      return processPosting()

    })
  })
  .then(async (result) => {
    
    await updatePostStatus(_users[_index].id, _users[_index].Posted += 1)
    console.log('Post Submitted')
    _imageCounter++;
    if(_users[_index].Posted == _postCounterLimit) {
       _imageCounter = 0
      _index += 1
      likePost()
    /*  await _chrome.kill()
      return startLogin()*/
  /*  setTimeout(function() {
        return startLogin()
    }, 10000)*/

    }

    setTimeout(function() {
        return processPosting()
    }, 10000)

  })
  .catch((error) => {

    console.log(error)
    return processPosting()

  })

}

module.exports = router
