var _chromeless

function AutoRegistration (chromeless) {
  _chromeless = chromeless
}

AutoRegistration.prototype.fullEmail = function (domain = 'gmail.com') {
    return this.email + '@' + domain
}

AutoRegistration.prototype.getRandomString = function (length) {

    var text = ""
    var possible = "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm1234567890"

    for(var i = 0; i < length; i++){
        text += possible.charAt(Math.floor(Math.random() * possible.length))
    }

    return text
}

AutoRegistration.prototype.proceed = function () {

  return new Promise(async (resolve, reject) => {
    await _chromeless
    .click('a.skip-link')
    .wait(5000)  //skip all the links
    .click('#phx-signup-form > div.textbox > div.prompt.select-username > div.suggestions > ul > li:nth-child(1) > button')
    .wait(5000)
    .click('a.skip-link')
    .wait(5000) //skip all the links
    .click('#page-container > div.content-main > div > div > div > div.Grid-cell.u-size4of10.StartCongratulations-leftCol > div > a')
    .wait(5000)  //skip all the links
    .click('#page-container > div.StartPageLayout.content-main > div > div > div.Grid-cell.u-size4of12 > div > div > button')
    .wait(5000) //skip all the links
    .click('#page-container > div.StartPageLayout.content-main > div > div > div.Grid-cell.u-size4of12 > div > div > a')
    .wait(5000)  //gmail no thanks
    .click('button.btn-follow-all')
    // .wait(5000) //follow button
    // .click('#push-notification-dialog-dialog > div:nth-child(6) > button')
    .wait(5000)
    .click('button.UIWalkthrough-skip.js-close')
    .wait(5000)
    // .exists('#search-query')
    .evaluate(() => {
      return document.querySelectorAll('#search-query').length > 0
    })
    .then((result) => {
      resolve(result)
    })
    .catch((error) => {
      console.log(error)
      resolve(false)

    }) 
    
  })
  .then((result) => {
    return result
  })

}

AutoRegistration.prototype.createAccount = function () {

  return new Promise(async (resolve, reject) => {

    this.fullname = this.getRandomString(10)
    this.email = "twt_" + this.getRandomString(11)
    this.password = "qwas1357**"

    await _chromeless
    .goto('https://twitter.com/signup?lang=ko')
    .wait(5000)
    .clearCache()
    .wait(5000)
    .clearCookies()
    .exists('div.Dropdown-menuContainer > div.Dropdown-menu.Dropdown--right > ul > li.js-logoutLink > button')
    .then(async (result) => {

      console.log('result of logout button: ' + result)

      if (result) {

        console.log('logged out.')

        await _chromeless
        .wait(5000)
        .evaluate(() => {
          document.querySelector('div.Dropdown-menuContainer > div.Dropdown-menu.Dropdown--right > ul > li.js-logoutLink > button').click()
        })
        .then(async () => {

          await _chromeless
          .wait(5000)
          .goto('https://twitter.com/signup?lang=ko')

          console.log('entering create account. ')

          return true

        })
        .catch((error) => {
          console.log(error)
        })

      } 

    })
    .catch((error) => {
      console.log(error)
      this.startRegistration()
    })

    await _chromeless
    .wait(5000)
    .clearInput('#full-name')
    .clearInput('#email')
    .clearInput('#password')
    .type(this.fullname, '#full-name')
    .wait(5000)
    .type(this.email + "@gmail.com", '#email')
    .wait(5000)
    .type(this.password, '#password')
    .wait(5000) //15000
    .click('#submit_button')
    .wait(5000) //10000
    .exists('#sms-phone-create-form > div.doit > div.links > a')
    .then(async (result) => {
        console.log(result)
        if (result) {

          let proceedResult = await this.proceed()

          resolve(proceedResult)

        } else {
          resolve(false)
        }
    })
    .catch(async (error) => {
      console.log(error)
      resolve(false)
    })
  })
  .then(async (result) => {
    console.log('fullname: ' + this.fullname)
    console.log('username: ' + this.fullEmail())
    console.log('password: ' + this.password)
    console.log('this is the login result: ' + result)
    if (!result) {

      console.log('logging out account. ')

      await _chromeless
      .exists('div.Dropdown-menuContainer > div.Dropdown-menu.Dropdown--right > ul > li.js-logoutLink > button')
      .then(async (result) => {

        console.log('result of logout button: ' + result)

        if (result) {

          console.log('logged out.')

          await _chromeless
          .wait(5000)
          .evaluate(() => {
            document.querySelector('div.Dropdown-menuContainer > div.Dropdown-menu.Dropdown--right > ul > li.js-logoutLink > button').click()
          })
          .then(() => {
            console.log('entering create account. ')
            // return this.startRegistration()
            return false
          })
          .catch((error) => {
            console.log(error)
          })
          
        } else {
          console.log('entering create account. ')
          // return this.startRegistration()
          return false
        }

      })
      
    } else {
      console.log('ended.')
      return true
    }

  })

}

AutoRegistration.prototype.stop = function () {

  return new Promise(async (resolve, reject) => { 
    let result = await _chromeless.end()
    resolve(result)
  })
  .then((result) => {
    console.log('AutoRegistration stopped.')
    return true
  })
  .catch((error) => {
    console.log(error)
    return true
  })
  
}

AutoRegistration.prototype.startRegistration = function () {

  return new Promise(async (resolve, reject) => {
    await _chromeless
    .goto('https://api.ipify.org/?format=json')
    .wait(5000)
    .evaluate(() => {
        return JSON.parse(document.body.innerText)
    })
    .then(async (result) => {
        console.log(result.ip)
        let createAccountResult = await this.createAccount()

        resolve(createAccountResult)

    })
    .catch((error) => {
      console.error('Search failed:', error)
      return this.startRegistration()
    })

  })
  .then((result) => {
    console.log('create account results: ' + result)
    return result
  })

}

module.exports = AutoRegistration